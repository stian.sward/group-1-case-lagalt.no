## Navbar

* Logo
    - Left of navbar
* Search box
    - Positioned at center
* Log in/sign up buttons
    - Positioned at right
    - Changes to Profile page / log out when signed in
* Drop-down menu
    - Positioned to the right of log in/sign up
    - About Lagalt
    - Sitemap?

## Main window

### Project cards

* Title
    - Left-orientated at top of card
* Description preview
    - Using introduction paragraph from main description, cuts off if necessary
* Image
    - Redering if present, otherwise description preview fills entire width of card
* Category
    - Bottom left of card
* Tags
    - Immediately to the right of category
* Date
    - Right-orientated at top of card

## Colors

_All colors defined in [color_palette.png](./color_palette.png)_