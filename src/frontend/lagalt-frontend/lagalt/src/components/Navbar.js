import React from 'react';

const Navbar = () => (
    <header id="navbar">
        <img id="logo" src="/images/logo_256.png" alt=""/>
        <input id="search-bar" className="" type="text" placeholder="Search"/>
        <div className="buttons">
            <button className="button">Log in</button>
            <button className="button">Register</button>
        </div>
    </header>
);

export default Navbar;