import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';

import ErrorView from './views/ErrorView';
import HomeView from './views/HomeView';
import LoginView from './views/LoginView';
import ProfileView from './views/ProfileView';
import ProjectView from './views/ProjectView';
import RegisterView from './views/RegisterView';
import SearchView from './views/SearchView';
import Navbar from './components/Navbar';

function App() {
  return (
    <Router>
      <Navbar/>
      <div className="content">
        <Switch>
          <Route path="/home" component={HomeView}/>
          <Route path="/login" component={LoginView}/>
          <Route path="/register" component={RegisterView}/>
          <Route path="/profile" component={ProfileView}/>
          <Route path="/project/:id" component={ProjectView}/>
          <Route path="/search/" component={SearchView}/>
          <Route path="/error" component={ErrorView}/>
          <Redirect exact from="/" to="/home"/>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
