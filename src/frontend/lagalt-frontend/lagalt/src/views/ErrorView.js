import React from 'react';

const ErrorView = (error) => (
    <ErrorView>
        <div className="error-box">
            <h1>Error</h1>
            <p>{error.message}</p>
        </div>
    </ErrorView>
);

export default ErrorView;